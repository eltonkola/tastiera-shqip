

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
 
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
 
public class TextToXml {
 
	 public static final Charset UTF8 = Charset.forName("UTF-8");
	
	  
	public static void main(String argv[]) {
 
	  try {
 
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
 
		// root elements
		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("wordlist");
		doc.appendChild(rootElement);
		
		File fileDir = new File("sq.txt");
		 
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileDir), UTF8));
		
		String line;
		int pos = 0;
		while ((line = br.readLine()) != null) {
		   // process the line.
			pos++;
			// staff elements
			Element staff = doc.createElement("w");
			
			String fjala = line;
			
			if(fjala.indexOf("/")>0){
				fjala = fjala.substring(0, fjala.indexOf("/"));
			}
			
			//krijoj noden
			staff.appendChild(doc.createTextNode(fjala));
			rootElement.appendChild(staff);
	 
			Attr attr = doc.createAttribute("f");
			
			int nr = numro(fjala);
			System.out.println( pos + " fjala " + fjala +" nr:" + nr);
			
			attr.setValue("" + nr);
			
			staff.setAttributeNode(attr);
			
			
		}
		br.close();
		
		
		
		
		
 
		
 
		
		
		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File("dict/words.xml"));
 
		// Output to console for testing
		// StreamResult result = new StreamResult(System.out);
 
		transformer.transform(source, result);
 
		System.out.println("File saved!");
 
	  } catch (ParserConfigurationException pce) {
		pce.printStackTrace();
	  } catch (TransformerException tfe) {
		tfe.printStackTrace();
	  } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	  }
	}
	
	
	
	private static int numro(String fjala){
		int nr = 1;
		try {
		 	File fileDir = new File("sq.txt");
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileDir), UTF8));
			String line;
			while ((line = br.readLine()) != null) {
				String ff = line;
				if(ff.indexOf("/")>0){
					ff = ff.substring(0, ff.indexOf("/"));
				}
				if(fjala.contains(ff))nr++;
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(nr>255)nr=255;
		return nr;
	}
	
	
}